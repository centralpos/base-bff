<?php

namespace Centralpos\BaseBff\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Http\Request;


class ValidateRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $abilities = ['phe-premium'];

        foreach ($abilities as $ability) {
            app(Gate::class)->authorize($ability);
        }

        return $next($request);
    }
}
